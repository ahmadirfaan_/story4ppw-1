from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Proyek(models.Model):
	judul_proyek = models.CharField(max_length=27)
	tanggal_mulai = models.CharField(max_length=27)
	tanggal_selesai = models.CharField(max_length=27)
	deskripsi_proyek = models.CharField(max_length=27)
	tempat_proyek = models.CharField(max_length=27)
	kategori_proyek = models.CharField(max_length=27)
