from django.shortcuts import render, redirect
from .forms import ProyekForm
from .models import Proyek

def index(request):
	proyek = Proyek.objects.all()

	context = {
		'judul_halaman':'List Proyek',
		'proyek':proyek
	}
	return render(request,'proyek/index.html', context)

def create(request):
	proyek_form = ProyekForm(request.POST or None)
	if request.method == 'POST' and proyek_form.is_valid():
		proyek_form.save()
		return redirect('../')

	context={
		'judul_halaman':'Pembuatan Proyek',
		'proyek_form':proyek_form,
	}
	return render(request, 'proyek/create.html', context)