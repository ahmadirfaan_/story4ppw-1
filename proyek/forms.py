from django import forms
from .models import Proyek

class ProyekForm(forms.ModelForm):
	class Meta:
		model = Proyek
		fields = ('judul_proyek','tanggal_mulai', 'tanggal_selesai', 'deskripsi_proyek', 'tempat_proyek', 'kategori_proyek')